const int notes[] PROGMEM = { 0,
  NOTE_C4, NOTE_CS4, NOTE_D4, NOTE_DS4, NOTE_E4, NOTE_F4, NOTE_FS4, NOTE_G4, NOTE_GS4, NOTE_A4, NOTE_AS4, NOTE_B4,
  NOTE_C5, NOTE_CS5, NOTE_D5, NOTE_DS5, NOTE_E5, NOTE_F5, NOTE_FS5, NOTE_G5, NOTE_GS5, NOTE_A5, NOTE_AS5, NOTE_B5,
  NOTE_C6, NOTE_CS6, NOTE_D6, NOTE_DS6, NOTE_E6, NOTE_F6, NOTE_FS6, NOTE_G6, NOTE_GS6, NOTE_A6, NOTE_AS6, NOTE_B6,
  NOTE_C7, NOTE_CS7, NOTE_D7, NOTE_DS7, NOTE_E7, NOTE_F7, NOTE_FS7, NOTE_G7, NOTE_GS7, NOTE_A7, NOTE_AS7, NOTE_B7
};


class Rtttl /*: public Printable */ {
private:
  const char * songPtr;
  bool needFree = false;
  const char * copyFPTR(const __FlashStringHelper* sourcePointer){
    if (!sourcePointer) return 0;
    const uint32_t len = strlen_P((PGM_P)sourcePointer);
    if ( len == 0) return 0;  
    char * newPointer = new char[len];
    needFree = true;
    strcpy_P(newPointer, (PGM_P)sourcePointer);
    return newPointer;
  };

    char * namePtr;  
    bool needFreeName = false;
public:

  const char * getName(){
    uint8_t index=0;
      while(name.pointer[index] != ':') {  // search end of name
        index++;  
      }
      uint8_t len = index+3;          // добавляем две "" и нуль символ
      namePtr = new char[len];
      needFreeName = true;
      namePtr[0] = '"'; namePtr[index+1] = '"';
      strncpy( namePtr+1, name.pointer, index);
      namePtr[index+2] = '\0';
    return namePtr;
  }

  struct Name : public Printable {
    const char * pointer;
    size_t printTo( Print& s) const {
      uint8_t index =0;
      size_t res = s.write('"');
      while( pointer[index] != ':') { // search end of name
        res += s.write( pointer[index] ); // print char
        index++;  
      }
      return res + s.write('"');  
    }
  };

  Name name;
  
  Rtttl(const char* pp) : songPtr(pp)
    {
    name.pointer = songPtr;
    }

  Rtttl(const __FlashStringHelper* pp_F) : songPtr(copyFPTR(pp_F))
    {
      if ( songPtr != 0 ) name.pointer = songPtr;
    }

  ~Rtttl(){   
    if (needFree) delete [] songPtr; // если копировали из флэш, то очищаем память
    if ( needFreeName ) delete [] namePtr;
    }
  const char * get(){
    return songPtr;
  }

  void play(){
    if ( songPtr == 0 ) return;
    const char * p = songPtr;
    while(*p != ':') { // ignore name
      p++;    
    }
    p++;   // skip ":"

  // Absolutely no error checking in here

  byte default_dur = 4;
  byte default_oct = 6;
  int bpm = 63;
  int num;
  long wholenote;
  long duration;
  byte note;
  byte scale;

  // format: d=N,o=N,b=NNN:
  // get default duration
  if(*p == 'd')
  {
    p++; p++;              // skip "d="
    num = 0;
    while(isdigit(*p))
    {
      num = (num * 10) + (*p++ - '0');
    }
    if(num > 0) default_dur = num;
    p++;                   // skip comma
  }

  // get default octave
  if(*p == 'o')
  {
    p++; p++;              // skip "o="
    num = *p++ - '0';
    if(num >= 3 && num <=7) default_oct = num;
    p++;                   // skip comma
  }

  // get BPM
  if(*p == 'b')
  {
    p++; p++;              // skip "b="
    num = 0;
    while(isdigit(*p))
    {
      num = (num * 10) + (*p++ - '0');
    }
    bpm = num;
    p++;                   // skip colon
  }

  // BPM usually expresses the number of quarter notes per minute
  wholenote = (60 * 1000L / bpm) * 4;  // this is the time for whole note (in milliseconds)

  // now begin note loop
  while(*p)
  {
    // first, get note duration, if available
    num = 0;
    while(isdigit(*p))
    {
      num = (num * 10) + (*p++ - '0');
    }
    
    if(num) duration = wholenote / num;
    else duration = wholenote / default_dur;  // we will need to check if we are a dotted note after

    // now get the note
    note = 0;

    switch(*p)
    {
      case 'c':
        note = 1;
        break;
      case 'd':
        note = 3;
        break;
      case 'e':
        note = 5;
        break;
      case 'f':
        note = 6;
        break;
      case 'g':
        note = 8;
        break;
      case 'a':
        note = 10;
        break;
      case 'b':
        note = 12;
        break;
      case 'p':
      default:
        note = 0;
    }
    p++;

    // now, get optional '#' sharp
    if(*p == '#')
    {
      note++;
      p++;
    }

    // now, get optional '.' dotted note
    if(*p == '.')
    {
      duration += duration/2;
      p++;
    }
  
    // now, get scale
    if(isdigit(*p))
    {
      scale = *p - '0';
      p++;
    }
    else
    {
      scale = default_oct;
    }

    scale += OCTAVE_OFFSET;

    if(*p == ',')
      p++;       // skip comma for next note (or we may be at the end)

    // now play the note

    if(note)
    {

      tone(TONE_PIN, notes[(scale - 4) * 12 + note]);
      delay(duration);
      noTone(TONE_PIN);
    }
    else
    {
      delay(duration);
    }
  }
}

#ifdef DEBUG
void play( Print& S ){
    const char * p = _p;
    S.write('"');
    while(*p != ':') { // ignore name
      S.write( *p);
      p++;    
    }
    S.write( '"'); S.write( *p);
    p++;   // skip ":"

  // Absolutely no error checking in here

  byte default_dur = 4;
  byte default_oct = 6;
  int bpm = 63;
  int num;
  long wholenote;
  long duration;
  byte note;
  byte scale;

  // format: d=N,o=N,b=NNN:
  // get default duration
  if(*p == 'd')
  {
    p++; p++;              // skip "d="
    num = 0;
    while(isdigit(*p))
    {
      num = (num * 10) + (*p++ - '0');
    }
    if(num > 0) default_dur = num;
    p++;                   // skip comma
  }

  S.print("ddur: "); S.println(default_dur, 10);

  // get default octave
  if(*p == 'o')
  {
    p++; p++;              // skip "o="
    num = *p++ - '0';
    if(num >= 3 && num <=7) default_oct = num;
    p++;                   // skip comma
  }

  S.print("doct: "); S.println(default_oct, 10);

  // get BPM
  if(*p == 'b')
  {
    p++; p++;              // skip "b="
    num = 0;
    while(isdigit(*p))
    {
      num = (num * 10) + (*p++ - '0');
    }
    bpm = num;
    p++;                   // skip colon
  }

  S.print("bpm: "); S.println(bpm, 10);

  // BPM usually expresses the number of quarter notes per minute
  wholenote = (60 * 1000L / bpm) * 4;  // this is the time for whole note (in milliseconds)

  S.print("wn: "); S.println(wholenote, 10);


  // now begin note loop
  while(*p)
  {
    // first, get note duration, if available
    num = 0;
    while(isdigit(*p))
    {
      num = (num * 10) + (*p++ - '0');
    }
    
    if(num) duration = wholenote / num;
    else duration = wholenote / default_dur;  // we will need to check if we are a dotted note after

    // now get the note
    note = 0;

    switch(*p)
    {
      case 'c':
        note = 1;
        break;
      case 'd':
        note = 3;
        break;
      case 'e':
        note = 5;
        break;
      case 'f':
        note = 6;
        break;
      case 'g':
        note = 8;
        break;
      case 'a':
        note = 10;
        break;
      case 'b':
        note = 12;
        break;
      case 'p':
      default:
        note = 0;
    }
    p++;

    // now, get optional '#' sharp
    if(*p == '#')
    {
      note++;
      p++;
    }

    // now, get optional '.' dotted note
    if(*p == '.')
    {
      duration += duration/2;
      p++;
    }
  
    // now, get scale
    if(isdigit(*p))
    {
      scale = *p - '0';
      p++;
    }
    else
    {
      scale = default_oct;
    }

    scale += OCTAVE_OFFSET;

    if(*p == ',')
      p++;       // skip comma for next note (or we may be at the end)

    // now play the note

    if(note)
    {
      S.print("Playing: ");
      S.print(scale, 10); S.print(' ');
      S.print(note, 10); S.print(" (");
      S.print(notes[(scale - 4) * 12 + note], 10);
      S.print(") ");
      S.println(duration, 10);
      tone(TONE_PIN, notes[(scale - 4) * 12 + note]);
      delay(duration);
      noTone(TONE_PIN);
    }
    else
    {
      S.print("Pausing: ");
      S.println(duration, 10);
      delay(duration);
    }
  }
}
#endif

};
